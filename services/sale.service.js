import SaleRepository from "../repositories/sale.repository.js";
import ClientRepository from "../repositories/client.repository.js";
import ProductRepository from "../repositories/product.repository.js";

async function createSale(sale) {
  const resClient = await ClientRepository.getClient(sale.client_id);
  const resProd = await ProductRepository.getProduct(sale.product_id);
  if (resClient.length == 0 || resProd.length == 0) {
    throw new Error("Client ou Product não encontrado."); //Se retornar
  }
  /*
  const resClient = await ClientRepository.getClient(sale.client_id);
  const resProd = await ProductRepository.getProduct(sale.product_id);
  const erro = [];
  if (resClient.length == 0) {
    erro.push("Client não encontrado.");
  }
  if (resProd.length == 0) {
    erro.push("Product não encontrado.");
  }
  if (erro) {
    throw erro;
  }
  */
  // console.log(resProd);
  if (parseInt(resProd.stock) <= 0) {
    throw new Error("Estoque indisponivel.");
  }
  resProd.stock--;
  await ProductRepository.updateProduct(resProd);
  return await SaleRepository.insertSale(sale);
}

async function getSales(prod) {
  if (prod) {
    return await SaleRepository.getSalesByProductID(prod);
  } else {
    return await SaleRepository.getSales();
  }
}
async function getSale(id) {
  return await SaleRepository.getSale(id);
}
async function deleteSale(id) {
  const sale = await SaleRepository.getSale(id);
  if (sale) {
    const prod = await SaleRepository.getProduct(sale.product_id);
    await SaleRepository.deleteSale(id);
    prod.stock--;
    await SaleRepository.updateProduct(prod);
  } else {
    throw new Error("Sale não encontrado.");
  }
}

async function updateSale(sale) {
  const resClient = await ClientRepository.getClient(sale.client_id);
  const resProd = await ProductRepository.getProduct(sale.product_id);
  const erro = [];
  if (resClient.length == 0) {
    erro.push("Client não encontrado.");
  }
  if (resProd.length == 0) {
    erro.push("Product não encontrado.");
  }
  if (erro) {
    throw erro;
  }
  return await SaleRepository.updateSale(sale);
}
export default {
  createSale,
  getSales,
  getSale,
  deleteSale,
  updateSale,
};
