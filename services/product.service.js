import productRepository from "../repositories/product.repository.js";
import supplierRepository from "../repositories/supplier.repository.js";

async function createProduct(product) {
  const res = await supplierRepository.getSupplier(product.supplier_id);

  if (res.length != 0) {
    return await productRepository.insertProduct(product);
  }
  throw new Error("O Supplier informado não existe.");
}

async function getProducts() {
  return await productRepository.getProducts();
}
async function getProduct(id) {
  return await productRepository.getProduct(id);
}
async function deleteProduct(id) {
  await productRepository.deleteProduct(id);
}

async function updateProduct(product) {
  //TRATAMENTO UTILIZADO NA VERSÃO NATIVA BD
  const res = await supplierRepository.getSupplier(product.supplier_id);
  if (res.length != 0) {
    return await productRepository.updateProduct(product);
  }
  throw new Error("O Supplier informado não existe.");
}
export default {
  createProduct,
  getProducts,
  getProduct,
  deleteProduct,
  updateProduct,
};
