import supplierService from "../services/supplier.service.js";

async function createSupplier(req, res, next) {
  try {
    const supplier = req.body;
    if (
      !supplier.name ||
      !supplier.cnpj ||
      !supplier.phone ||
      !supplier.email ||
      !supplier.address
    ) {
      throw new Error("Name, CNPJ, Phone e Address são obrigatorios.");
    }
    res.send(await supplierService.createSupplier(supplier));
    logger.info(`POST/Supplier - ${JSON.stringify(supplier)}`);
  } catch (err) {
    next(err);
  }
}

async function getSuppliers(req, res, next) {
  try {
    res.send(await supplierService.getSuppliers());
    logger.info("GET/Suppliers");
  } catch (err) {
    next(err);
  }
}

async function getSupplier(req, res, next) {
  try {
    res.send(await supplierService.getSupplier(req.params.id));
    logger.info("GET/Supplier - ID");
  } catch (err) {
    next(err);
  }
}
async function deleteSupplier(req, res, next) {
  try {
    await supplierService.deleteSupplier(req.params.id);
    res.end();
    logger.info("DELETE/Supplier - ID");
  } catch (err) {
    next(err);
  }
}

async function updateSupplier(req, res, next) {
  try {
    let supplier = req.body;
    if (
      !supplier.supplier_id ||
      !supplier.name ||
      !supplier.cnpj ||
      !supplier.phone ||
      !supplier.email ||
      !supplier.address
    ) {
      throw new Error("ID, Name, CNPJ, Phone e Address são obrigatorios.");
    }
    supplier = await supplierService.updateSupplier(supplier);
    res.send(supplier);
    logger.info(`PUT/Supplier - ${JSON.stringify(supplier)}`);
  } catch (err) {
    next(err);
  }
}

export default {
  createSupplier,
  getSupplier,
  getSuppliers,
  deleteSupplier,
  updateSupplier,
};
