import productService from "../services/product.service.js";

async function createProduct(req, res, next) {
  try {
    const product = req.body;
    if (
      !product.name ||
      !product.description ||
      !product.value ||
      !product.stock ||
      !product.supplier_id
    ) {
      throw new Error(
        "Name, description, value, stock e supplier são obrigatorios."
      );
    }
    res.send(await productService.createProduct(product));
    logger.info(`POST/Product - ${JSON.stringify(product)}`);
  } catch (err) {
    next(err);
  }
}

async function getProducts(req, res, next) {
  try {
    res.send(await productService.getProducts());
    logger.info("GET/Products");
  } catch (err) {
    next(err);
  }
}

async function getProduct(req, res, next) {
  try {
    res.send(await productService.getProduct(req.params.id));
    logger.info("GET/Product - ID");
  } catch (err) {
    next(err);
  }
}
async function deleteProduct(req, res, next) {
  try {
    await productService.deleteProduct(req.params.id);
    res.end();
    logger.info("DELETE/Product - ID");
  } catch (err) {
    next(err);
  }
}

async function updateProduct(req, res, next) {
  try {
    let product = req.body;
    if (
      !product.product_id ||
      !product.name ||
      !product.description ||
      !product.value ||
      !product.stock ||
      !product.supplier_id
    ) {
      throw new Error(
        "ID, description, value, stock e supplier são obrigatorios."
      );
    }
    product = await productService.updateProduct(product);
    res.send(product);
    logger.info(`PUT/Product - ${JSON.stringify(product)}`);
  } catch (err) {
    next(err);
  }
}

export default {
  createProduct,
  getProduct,
  getProducts,
  deleteProduct,
  updateProduct,
};
